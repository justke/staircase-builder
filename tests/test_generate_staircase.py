from unittest import TestCase

from src.generate_staircase import generate_staircase


class TestPrintStaircase(TestCase):

    maxDiff = None

    def test_happy_path(self):
        expected_result = (
            ' \n'
            '#'
        )
        self.assertEqual(expected_result, generate_staircase(1))

    def test_zero_case(self):
        expected_result = ''
        self.assertEqual(expected_result, generate_staircase(0))

    def test_larger_number(self):
        expected_result = (
            '          \n'
            '         #\n'
            '        ##\n'
            '       ###\n'
            '      ####\n'
            '     #####\n'
            '    ######\n'
            '   #######\n'
            '  ########\n'
            ' #########\n'
            '##########'
        )
        self.assertEqual(expected_result, generate_staircase(10))
