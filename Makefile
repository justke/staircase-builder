.DEFAULT_GOAL := help
.PHONY: tests

# General Commands
help:
	cat Makefile

install:
	pipenv install --ignore-pipfile

dev:
	pipenv install --dev

no-lock:
	pipenv install --skip-lock

clean:
	pipenv --rm

fresh: clean install

# Development Commands
tests:
	pipenv run pytest tests

test: tests

lint:
	pipenv run prospector

types:
	pipenv run mypy ./src

coverage:
	pipenv run coverage run -m unittest discover tests;
	pipenv run coverage report

format:
	pipenv run yapf -i *.py **/*.py

format-check:
	pipenv run yapf --diff *.py **/*.py

qa: lint types tests format-check

# Application Specific Commands
steps:
	pipenv run python src/generate_staircase.py
