# -*- coding: utf-8 -*-
def generate_staircase(num_steps: int) -> str:
    """
    A simple function that will print to the console a staircase made out of hash symbols based on a number passed in
    to the function for how many stairs the staircase will have.

    :param num_steps: (Int) - The number of steps to print out for the staircase
    :return: (str) - This function returns a string of a staircase that can be printed
    """
    staircase = []
    for number in range(num_steps + 1):
        spaces = " " * (num_steps - number)
        stairs = "#" * number
        staircase.append(f'{spaces}{stairs}')
    return '\n'.join(staircase)


if __name__ == "__main__":
    print(generate_staircase(10))
